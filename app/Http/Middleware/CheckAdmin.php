<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //echo session()->pull('logged_in', 'default');die;
        if (session()->pull('logged_in', 'default') != 'admin') {
            return redirect()->route('admins.login');
        }
        return $next($request);
    }
}

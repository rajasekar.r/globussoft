<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function index(){
        return view('admin.dashboard');
    }

    public function login(){
        
        if (session()->pull('logged_in', 'default') == 'admin') {
            return redirect()->route('admins.dashboard');
        }
        return view('admin.login');
    }

    public function handleLogin(Request $req){
        $validatedData = $req->validate([
            'email' => 'required|email',
            'password' => 'required'
        ], [
            'email.required' => 'Email is required',
            'password.required' => 'Password is required'

        ]);
        $credentials = $req->only('email', 'password');
        $remember_me  = ( !empty( $req->remember_me ) )? TRUE : FALSE;
        if(Auth::guard('webadmin')->attempt($credentials)){
            $admin = Admin::where(["email" => $credentials['email']])->first();
            session()->put('logged_in', 'admin');
           return redirect()->route('admins.dashboard');
        }else{
            return redirect()->route('admins.login')->with('failure','Wrong credentials, Please enter correct credentials!');
        }
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('admins.login');
    }

    public function registration(){
        return view('admin.register');
    }

    public function storeadmin(Request $request){

        $validatedData = $request->validate([
            'name' => 'required|alpha',
            'password' => 'required|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%_@]).*$/',
            'email' => 'required|email|unique:users'
        ], [
            'name.required' => 'Name is required',
            'password.required' => 'Password is required',
            'password.min' => 'Password length minimum 8 character',
            'password.regex' => 'Password must contain at least one uppercase letter, one special symbol, and one number'

        ]);


        $name = $request->input('name');
        $email = $request->input('email');
        $input_pass = $request->input('password');
        $password = Hash::make($input_pass);
        
        $registerdetails = new Admin();
        $registerdetails->name = $name;
        $registerdetails->email = $email;
        $registerdetails->password = $password;
        $registerdetails->save();
       
        return redirect()->route('admins.login')->with('success','Admin Registered Successfully');

    }
}

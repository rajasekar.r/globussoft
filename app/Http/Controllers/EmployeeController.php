<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $semployees = Employee::all();
        return view('admin.employee.index')->with('employees', $semployees); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.employee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $input_pass = $request->input('password');
        $password = Hash::make($input_pass);
        $mobile = $request->input('mobile');
        $designation = $request->input('designation');
        $salary = $request->input('salary');
        
        $registerdetails = new Employee();
        $registerdetails->name = $name;
        $registerdetails->email = $email;
        $registerdetails->password = $password;
        $registerdetails->mobile = $mobile;
        $registerdetails->designation = $designation;
        $registerdetails->price = $salary;
        $registerdetails->save();

        return response()->json(['success'=>'Employee Added Successfully Using Ajax.']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $semployee = Employee::where('id', $id)->get();
        return view('admin.employee.edit')->with('employee',$semployee);
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');

        $empdetails = Employee::where('id', $id)->firstOrFail();

        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');
        $mobile = $request->input('mobile');
        $designation = $request->input('designation');
        $salary = $request->input('salary');
        
        $empdetails->name = $name;
        $empdetails->email = $email;
        $empdetails->password = $password;
        $empdetails->mobile = $mobile;
        $empdetails->designation = $designation;
        $empdetails->price = $salary;
        $empdetails->save();

        return response()->json(['success'=>'Record Updated Successfully Using Ajax.']);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {   
        $id = $request->input('emp_id');
        $employee = Employee::find($id);
        $employee->delete();

        return response()->json(['success'=>'Record Deleted Successfully Using Ajax.']);
    }

    public function export(){
        return Excel::download(new Employee, 'employees.xlsx');
    }

    public function login(){
        
        if (session()->pull('logged_in', 'default') == 'admin') {
            return redirect()->route('employees.dashboard');
        }
        return view('employee.login');
    }

    public function handleLogin(Request $req){
        $validatedData = $req->validate([
            'email' => 'required|email',
            'password' => 'required'
        ], [
            'email.required' => 'Email is required',
            'password.required' => 'Password is required'

        ]);
        $credentials = $req->only('email', 'password');

        $remember_me  = ( !empty( $req->remember_me ) )? TRUE : FALSE;
       
        if(Auth::guard('website')->attempt($credentials)){
            $admin = Employee::where(["email" => $credentials['email']])->first();
            session()->put('logged_in', 'site');
           return redirect()->route('employees.dashboard');
        }else{
            return redirect()->route('employees.login')->with('failure','Wrong credentials, Please enter correct credentials!');
        }
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('employees.login');
    }

    public function registration(){
        return view('employee.register');
    }



    public function storeemployeesite(Request $request){
        
        $name = $request->input('name');
        $email = $request->input('email');
        $input_pass = $request->input('password');
        $password = Hash::make($input_pass);
        $mobile = $request->input('mobile');
        $designation = $request->input('designation');
        $salary = $request->input('salary');
        
        $registerdetails = new Employee();
        $registerdetails->name = $name;
        $registerdetails->email = $email;
        $registerdetails->password = $password;
        $registerdetails->mobile = $mobile;
        $registerdetails->designation = $designation;
        $registerdetails->price = $salary;
        $registerdetails->save();

        return redirect()->route('employees.login')->with('success','Employee Record Created Successfully!');
    }

    public function dashboard(){
        return view('employee.dashboard');
    }




}

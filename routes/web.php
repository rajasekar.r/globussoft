<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/',[AdminController::class,'login']);
Route::get('/admin/login',[AdminController::class,'login'])->name('admins.login');
Route::post('/admin/handlelogin',[AdminController::class,'handleLogin'])->name('admins.handleLogin');
Route::get('/admin/dashboard',[AdminController::class,'index'])->name('admins.dashboard')->middleware('checkadmin');
Route::get('/admin/logout',[AdminController::class,'logout'])->name('admins.logout');

Route::get('/admin/registration/',[AdminController::class,'registration'])->name('admins.registration');
Route::post('/admin/storeadmin/',[AdminController::class,'storeadmin'])->name('admins.storeadmin');

//Employee
Route::get('/admin/listemployee/',[EmployeeController::class,'index'])->name('admins.listemployee');
Route::get('/admin/addemployee/',[EmployeeController::class,'create'])->name('admins.addemployee');
Route::post('/admin/storeemployee/',[EmployeeController::class,'store'])->name('admins.storeemployee');
Route::get('/admin/editemployee/{prod_id}',[EmployeeController::class,'edit'])->name('admins.editemployee');
Route::post('/admin/updateemployee/',[EmployeeController::class,'update'])->name('admins.updateemployee');
Route::post('/admin/deleteemployee/',[EmployeeController::class,'destroy'])->name('admins.deleteemployee');
Route::get('/admin/exportemployee/',[EmployeeController::class,'export'])->name('admins.exportemployee');


//Site

Route::get('/employee/login',[EmployeeController::class,'login'])->name('employees.login');
Route::post('/employee/handlelogin',[EmployeeController::class,'handleLogin'])->name('employees.handleLogin');
Route::get('/employee/registration/',[EmployeeController::class,'registration'])->name('employees.registration');
Route::post('/employee/storeemployee/',[EmployeeController::class,'storeemployeesite'])->name('employees.storeemployeesite');
Route::get('/employee/dashboard',[EmployeeController::class,'dashboard'])->name('employees.dashboard');

//Post
Route::get('/home', [App\Http\Controllers\PostController::class, 'index'])->name('post.home');
Route::get('post/create', [App\Http\Controllers\PostController::class, 'create'])->name('post.create');
Route::post('post', [App\Http\Controllers\PostController::class, 'store'])->name('post.store');
Route::get('post/{post}/edit', [App\Http\Controllers\PostController::class, 'edit'])->name('post.edit');
Route::get('post/{post}', [App\Http\Controllers\PostController::class, 'show'])->name('post.show');
Route::put('post/{post}', [App\Http\Controllers\PostController::class, 'update'])->name('post.update');
Route::delete('post/{post}', [App\Http\Controllers\PostController::class, 'destroy'])->name('post.delete');

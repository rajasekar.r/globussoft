@extends('admin.layouts.app')

@section('content')
<div class="content-wrapper">
        <div class="content">	
            <div class="row">
                <div class="col-12">
                  <!-- Recent Order Table -->
                  <div class="card card-table-border-none recent-orders" id="recent-orders">
                    <div class="card-header justify-content-between">
                      <h2> Product View</h2>
                    </div>
                    <div class="card-body pt-0 pb-5">
                      Name : {{ $product[0]->name }} </br>
                      Email : {{ $product[0]->price }} </br>
                      Created At : {{ $product[0]->created_at }} </br>
                      Updated At : {{ $product[0]->updated_at }} </br>
                      Image :
                      @if($product[0]->image!='') <img src="{{ url($product[0]->image) }}" height="60px" width="50px">
                                                    @else  
                                                    <img src="{{ asset('admin/img/no-img.jpg') }}" height="60px" width="50px">
                                                @endif 
                    </div>
                  </div>
</div>
							</div>
@push('scripts')

@endpush
@endsection
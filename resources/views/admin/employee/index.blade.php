@extends('admin.layouts.app')

@section('content')
<div class="content-wrapper">
        <div class="content">	
            <div class="row">
                <div class="col-12">
                  <!-- Recent Order Table -->
                  <div class="card card-table-border-none recent-orders" id="recent-orders">
                    <div class="card-header justify-content-between">
                      <h2>Employee List</h2>
                      <!--<a style="color:blue;" href="{{ route('admins.exportemployee') }}">Export Employee Data</a>-->
                    </div>
                    <div class="card-body pt-0 pb-5">
                      <table class="table card-table table-responsive table-responsive-large" style="width:100%">
                        <thead>
                          <tr class="table-striped">
                            <th class="d-none d-lg-table-cell">Name</th>
                            <th class="d-none d-lg-table-cell">Email</th>
                            <th class="d-none d-lg-table-cell">Mobile</th>
                            <th class="d-none d-lg-table-cell">Designation</th>
                            <th class="d-none d-lg-table-cell">Salary</th>
                            <th class="d-none d-lg-table-cell">Created At</th>

                           
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($employees as $key=>$employee)
                          <tr>
                            <td class="d-none d-lg-table-cell">{{ $employee->name  }}</td>
                            <td class="d-none d-lg-table-cell">{{ $employee->email  }}</td>
                            <td class="d-none d-lg-table-cell">{{ $employee->mobile  }}</td>
                            <td class="d-none d-lg-table-cell">{{ $employee->designation  }}</td>
                            <td class="d-none d-lg-table-cell">{{ $employee->price  }}</td>

                            <td class="d-none d-lg-table-cell">{{ $employee->created_at->diffForHumans()  }}</td>

                            <td class="text-right">
                              <div class="dropdown show d-inline-block widget-dropdown">
                                <a class="dropdown-toggle icon-burger-mini" href="" role="button" id="dropdown-recent-order1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static"></a>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-recent-order1">
                                  <li class="dropdown-item">
                                    <a href="{{ url('/admin/editemployee') }}/{{ $employee->id }}">Edit</a>
                                  </li>
                                  <li class="dropdown-item">
                                    <a class="delete_btn" emp_id="{{ $employee->id }}">Delete</a>
                                  </li>
                                </ul>
                              </div>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
</div>
							</div>


              <script>
$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
$(document).ready(function(){
  $(".delete_btn").click(function(){
    var emp_id = $(this).attr('emp_id');
    $.ajax({
           type:'POST',
           url:"{{ route('admins.deleteemployee') }}",
           data:{"_token": "{{ csrf_token() }}",
           emp_id:emp_id
					},
           success:function(data){
              alert(data.success);
			        location.reload();
           }
        });
    
  });
});

</script>

@push('scripts')
@endpush
@endsection
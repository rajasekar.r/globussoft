@extends('admin.layouts.app')

@section('content')

<div class="content-wrapper">
        <div class="content">	
        <div class="row">
								<div class="col-lg-9">
									<div class="card card-default">
										<div class="card-header card-header-border-bottom">
											<h2>Edit Product</h2>
										</div>
										<div class="card-body">
                                        <form>
												<div class="form-group">
													<label for="exampleFormControlInput1">Name</label>
													<input type="text" class="form-control" id="name" name="name"  value="{{ $employee[0]->name }}" placeholder="Enter Name" required>
													
													<input type="hidden" class="form-control" id="hidd_id" name="hidd_id"  value="{{ $employee[0]->id }}" >
												</div>
												<div class="form-group">
													<label for="exampleFormControlInput1">Email</label>
													<input type="text" class="form-control" id="email" name="email"  value="{{ $employee[0]->email }}" placeholder="Enter Email" required>
												</div>
												
												<div class="form-group">
													<label for="exampleFormControlInput1">Password</label>
													<input type="text" class="form-control" id="password" name="password"  value="{{ $employee[0]->password }}" placeholder="Enter Password" required>
												</div>

												<div class="form-group">
													<label for="exampleFormControlInput1">Mobile</label>
													<input type="text" class="form-control" id="mobile" name="mobile"  value="{{ $employee[0]->mobile }}" placeholder="Enter Mobile" required>
												</div>

												<div class="form-group">
													<label for="exampleFormControlInput1">Designation</label>
													<input type="text" class="form-control" id="designation" name="designation"  value="{{ $employee[0]->designation }}" placeholder="Enter Designation" required>
												</div>

												<div class="form-group">
													<label for="exampleFormControlInput1">Salary</label>
													<input type="text" class="form-control" id="salary" name="salary"value="{{ $employee[0]->price }}"  placeholder="Enter Product Price" required>
												</div>

												<div class="form-footer pt-4 pt-5 mt-4 border-top">
													<input type="button" class="btn btn-primary btn-default" id="submit_button" value="Submit"> 
												</div>
											</form>
										</div>
									</div>
								</div>
							
							</div>

							<script>
$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
$(document).ready(function(){
  $("#submit_button").click(function(){
    var name = $('#name').val();
    var email = $('#email').val();
    var password = $('#password').val();
    var mobile = $('#mobile').val();
    var designation = $('#designation').val();
    var salary = $('#salary').val();
    var id = $('#hidd_id').val();


    $.ajax({
           type:'POST',
           url:"{{ route('admins.updateemployee') }}",
           data:{"_token": "{{ csrf_token() }}",
		   			name:name,
		   			email:email,
					password:password,
					mobile:mobile,
					designation:designation,
					salary:salary,
					id:id
					},
           success:function(data){
              alert(data.success);
			  location.reload();
           }
        });
    
  });
});

</script>

@endsection
@extends('admin.layouts.app')

@section('content')

<div class="content-wrapper">
        <div class="content">	
        <div class="row">
								<div class="col-lg-9">
									<div class="card card-default">
										<div class="card-header card-header-border-bottom">
											<h2>Create Employee</h2>
										</div>
										<div class="card-body">
                                        <form>
												<div class="form-group">
													<label for="exampleFormControlInput1">Name</label>
													<input type="text" class="form-control" id="name" name="name" placeholder="Enter Product Name" required>
												</div>

												<div class="form-group">
													<label for="exampleFormControlInput1">Email</label>
													<input type="text" class="form-control" id="email" name="email" placeholder="Enter Email" required>
												</div>

												<div class="form-group">
													<label for="exampleFormControlInput1">Password</label>
													<input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" required>
												</div>

												<div class="form-group">
													<label for="exampleFormControlInput1">Mobile</label>
													<input type="text" class="form-control" id="mobile" name="mobile" placeholder="Enter Mobile" required>
												</div>

												<div class="form-group">
													<label for="exampleFormControlInput1">Designation</label>
													<input type="text" class="form-control" id="designation" name="designation" placeholder="Enter Designtion" required>
												</div>

												<div class="form-group">
													<label for="exampleFormControlInput1">Salary</label>
													<input type="text" class="form-control" id="salary" name="salary" placeholder="Enter Salary" required>
												</div>

												<div class="form-footer pt-4 pt-5 mt-4 border-top">
													<input type="button" class="btn btn-primary btn-default" id="submit_button" value="Submit"> 
													<!--<button type="submit" id="submit_button" class="btn btn-primary btn-default">Submit</button>-->
												</div>
											</form>
										</div>
									</div>
								</div>
							
							</div>

<script>
$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
$(document).ready(function(){
  $("#submit_button").click(function(){
    var name = $('#name').val();
    var email = $('#email').val();
    var password = $('#password').val();
    var mobile = $('#mobile').val();
    var designation = $('#designation').val();
    var salary = $('#salary').val();


    $.ajax({
           type:'POST',
           url:"{{ route('admins.storeemployee') }}",
           data:{"_token": "{{ csrf_token() }}",
		   			name:name,
		   			email:email,
					password:password,
					mobile:mobile,
					designation:designation,
					salary:salary
					},
           success:function(data){
              alert(data.success);
			  location.reload();
           }
        });
    
  });
});

</script>
@endsection
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="description" content="Sleek Dashboard - Free Bootstrap 4 Admin Dashboard Template and UI Kit. It is very powerful bootstrap admin dashboard, which allows you to build products like admin panels, content management systems and CRMs etc.">


  <title>Globussoft Project</title>

  <!-- GOOGLE FONTS -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500|Poppins:400,500,600,700|Roboto:400,500" rel="stylesheet" />
  <link href="https://cdn.materialdesignicons.com/4.4.95/css/materialdesignicons.min.css" rel="stylesheet" />


  <!-- PLUGINS CSS STYLE -->
  <link href="{{ asset('admin/plugins/nprogress/nprogress.css') }}" rel="stylesheet" />
  
  <link href="{{ asset('admin/plugins/jvectormap/jquery-jvectormap-2.0.3.css') }}" rel="stylesheet" />
  
  <link href="{{ asset('admin/plugins/toastr/toastr.min.css') }}" rel="stylesheet" />

  <!-- SLEEK CSS -->
  <link id="sleek-css" rel="stylesheet" href="{{ asset('admin/css/sleek.css') }}" />

  <!-- FAVICON -->
  <link href="{{ asset('admin/img/GLobussoft-logo.png') }}" rel="shortcut icon" />

  <!--
    HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries
  -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script src="{{ asset('admin/plugins/nprogress/nprogress.js') }}"></script>
  <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <style>
      .error{
          color: red;
      }
  </style>
</head>

<?php
        $section = Request::segment(1);
        $method     = Request::segment(2);
    ?>

<?php
    ?>
<body class="header-fixed sidebar-fixed sidebar-dark header-light" id="body">
  
  <script>
    NProgress.configure({ showSpinner: false });
    NProgress.start();
  </script>

  
  <div id="toaster"></div>
  

  <div class="wrapper">

            <!--
          ====================================
          ——— LEFT SIDEBAR WITH FOOTER
          =====================================
        -->
        <aside class="left-sidebar bg-sidebar">
          <div id="sidebar" class="sidebar sidebar-with-footer">
            <!-- Aplication Brand -->
            <div class="app-brand">
              <a href="{{ route('admins.dashboard') }}" title="Sleek Dashboard">
          
                <span class="brand-name text-truncate">Globussoft Project</span>
              </a>
            </div>
            <!-- begin sidebar scrollbar -->
            <div class="sidebar-scrollbar">

              <!-- sidebar menu -->
              <ul class="nav sidebar-inner" id="sidebar-menu">
                

                
                  <li  class="">
                    <a class="sidenav-item-link" href="{{ route('admins.dashboard') }}" >
                      <i class="mdi mdi-view-dashboard-outline"></i>
                      <span class="nav-text">Dashboard</span> 
                    </a>
                  </li>


                  <li  class="has-sub @if($method=='listemployee' | $method=='addemployee') active expand @endif">
                    <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#employee"
                      aria-expanded="false" aria-controls="employee">
                      <i class="mdi mdi-account-group-outline"></i>
                      <span class="nav-text">Employee</span> <b class="caret"></b>
                    </a>
                    <ul  class="collapse @if($method=='listemployee' | $method=='addemployee') show @endif"  id="employee"
                      data-parent="#sidebar-menu">
                      <div class="sub-menu">
                          
                            <li class="@if($method=='listemployee') active @endif">
                              <a class="sidenav-item-link" href="{{ Route('admins.listemployee') }}">
                                <span class="nav-text">Employee List</span>
                              </a>
                            </li>
                            <li class="@if($method=='addemployee') active @endif">
                              <a class="sidenav-item-link" href="{{ Route('admins.addemployee') }}">
                                <span class="nav-text">Add Employee</span>
                              </a>
                            </li>
                            
                      </div>
                    </ul>
                  </li>


                  

              </ul>

            </div>
          </div>
        </aside>
        

    <div class="page-wrapper">
                <!-- Header -->
          <header class="main-header " id="header">
            <nav class="navbar navbar-static-top navbar-expand-lg">
              <!-- Sidebar toggle button -->
              <button id="sidebar-toggler" class="sidebar-toggle">
                <span class="sr-only">Toggle navigation</span>
              </button>
              <!-- search form -->
              <div class="search-form d-none d-lg-inline-block">
              
               
              </div>

              <div class="navbar-right ">
                <ul class="nav navbar-nav">
                
                  <!-- User Account -->
                  <li class="dropdown user-menu">
                    <button href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                      <span class="d-none d-lg-inline-block">Admin</span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                      <li class="dropdown-footer">
                        <a  href="{{ route('admins.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="mdi mdi-logout"></i> Log Out </a>
                        <form id="logout-form" action="{{ route('admins.logout') }}" method="get" style="display: none;">
                        {{ csrf_field() }}
                        </form>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </nav>


          </header>
    @if(session()->has('success'))
    <div class="alert alert-success success_msg_div" >
        {{ session()->get('success') }}
    </div>
    @endif
    @yield('content')

    @include( 'admin.layouts.footer' )
  

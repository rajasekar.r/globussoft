<!DOCTYPE html> 
  <html lang="en"> 
    <head>  
      <meta charset="UTF-8">  
      <meta name="viewport" content="width=device-width, initial-scale=1.0">                
      <title>Admin Login</title>  
  
  <!-- GOOGLE FONTS -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500|Poppins:400,500,600,700|Roboto:400,500" rel="stylesheet" />
  <link href="https://cdn.materialdesignicons.com/4.4.95/css/materialdesignicons.min.css" rel="stylesheet" />


  <!-- PLUGINS CSS STYLE -->
  <link href="{{ asset('admin/plugins/nprogress/nprogress.css') }}" rel="stylesheet" />
  
  <link href="{{ asset('admin/plugins/jvectormap/jquery-jvectormap-2.0.3.css') }}" rel="stylesheet" />
  
  <link href="{{ asset('admin/plugins/toastr/toastr.min.css') }}" rel="stylesheet" />

  <!-- SLEEK CSS -->
  <link id="sleek-css" rel="stylesheet" href="{{ asset('admin/css/sleek.css') }}" />

  <!-- FAVICON -->
  <link href="{{ asset('admin/img/GLobussoft-logo.png') }}" rel="shortcut icon" />

  <!--
    HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries
  -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script src="{{ asset('admin/plugins/nprogress/nprogress.js') }}"></script>
  <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
 <style>
body {  
  background: #b9a5e185; 
}
 form {  
   width: 30%;  
   margin: 60px auto;  
   background: #efefef;  
   padding: 60px 120px 80px 120px;  
   text-align: center;  
   -webkit-box-shadow: 2px 2px 3px rgba(0,0,0,0.1); 
   box-shadow: 2px 2px 3px rgba(0,0,0,0.1); 
}

label {  
  display: block;  
  position: relative;  
  margin: 40px 0px; 
}
 .label-txt {  
   position: absolute;  
   top: -1.6em;  
   padding: 10px;  
   font-family: sans-serif;  
   font-size: .8em;  
   letter-spacing: 1px;  
   color: rgb(120,120,120);  
   transition: ease .3s; 
}
 .input {  
   width: 100%;  
   padding: 10px;  
   background: transparent;  
   border: none;  
   outline: none; 
}
.line-box {  
  position: relative;  
  width: 100%;  
  height: 2px;  
  background: #BCBCBC; 
}
 .line {
   position: absolute;  
   width: 0%;  
   height: 2px;  
   top: 0px;  
   left: 50%;  
   transform: translateX(-50%);  
   background: #8BC34A;  
   transition: ease .6s; 
}  
 .input:focus + .line-box .line {  
   width: 100%; 
}
 .label-active {  
   top: -3em; 
}
 button {  
   display: inline-block; 
   padding: 12px 24px;  
   background: rgb(220,220,220);  
   font-weight: bold;  
   color: rgb(120,120,120);  
   border: none;  outline: none;  
   border-radius: 3px;  
   cursor: pointer;  
   transition: ease .3s; 
}  
 button:hover {  
   background: #8BC34A;  
   color: #ffffff; 
}
.failure_msg_div{
  font-weight: bold;
  color:red;
  padding-left: 800px;
  font-size: 20px;
}

.error_msg_div{
  font-weight: bold;
  color:red;
  padding-left: 800px;
  font-size: 20px;
}
</style>

    </head>
    @if(session()->has('success'))
    <div class="alert alert-success success_msg_div" >
        {{ session()->get('success') }}
    </div>
    @endif

    @if(session()->has('failure'))
    <div class="alert alert-failure failure_msg_div" >
        {{ session()->get('failure') }}
    </div>
    @endif
    <body> 
        <div>   
        @if (count($errors) > 0) 
          <div class = "alert alert-danger error_msg_div"> 
          <ul> @foreach ($errors->all() as $error) 
            <li>{{ $error }}</li>
               @endforeach 
          </ul>
           
          </div>
        @endif
          <form method="post" action="{{ route('admins.handleLogin') }}" enctype="multipart/form-data">   
          {{ csrf_field() }} 
            <h4 class="text-warning text-center pt-5">Admin Login</h4>  
            <label>     
              <input type="text" class="input" name="email" placeholder="EMAIL" />                   
              <div class="line-box">          
              <div class="line"></div>        
              </div>    
            </label>   
            <label>
              <input type="password" class="input" name="password" placeholder="PASSWORD" />        
              <div class="line-box">          
                <div class="line"></div>        
              </div>    
            </label>

            <label>
              <input type="checkbox" checked="checked" name="remember_me"> Remember me
            </label>

            <button type="submit">Submit</button> 
            <button type="submit"><a href="{{ route('admins.registration') }}">Click Here To Register Admin</a></button>  
          </form> 
      </div> 
    </body> 
    <script>
    $(document).ready(function(){
      setTimeout(function(){ $('.failure_msg_div').hide(); }, 2000);
      setTimeout(function(){ $('.success_msg_div').hide(); }, 2000);
    });
</script>
</html>